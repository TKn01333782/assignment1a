﻿<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

    <title>Gym Membership</title>
     XYZ GYM
    <br />
    <br />

   <asp:h1>Best Gym in Town</asp:h1><br />
    <body>

    <form id="form1" runat="server">
      
            <asp:Label runat="server" ID="firstName">First Name</asp:Label><asp:TextBox runat="server" ID="nametextbox" placeholder="e.g.john abc"></asp:TextBox>
          <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Name" ControlToValidate="nametextbox" ID="validatorName"></asp:RequiredFieldValidator>
            <br /><br />
            <asp:Label runat="server" ID="lastname">Last Name</asp:Label><asp:TextBox runat="server" ID="lastnametextbox"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a  Last Name" ControlToValidate="lastnametextbox" ID="RequiredFieldValidator1"></asp:RequiredFieldValidator>
            <br /><br />
            <asp:Label runat="server" ID="addres">Address </asp:Label><asp:TextBox runat="server" ID="Address" placeholder="Address"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Provide Address" ControlToValidate="Address" ID="RequiredFieldValidator"></asp:RequiredFieldValidator>
            <br />
            <asp:Label runat="server" ID="postalcode">Postal Code</asp:Label><asp:TextBox runat="server" ID="postalcodetextbox" Placeholder="L4T1D6"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter Postal Code" ControlToValidate="postalcodetextbox" ID="RequiredFieldValidator3"></asp:RequiredFieldValidator>
            <br /><br />
            <asp:TextBox runat="server" ID="clientEmail" placeholder="E.g.xyz@abc.com"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter an Email" ControlToValidate="clientEmail" ID="RequiredFieldValidator2"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="clientEmail" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>
            <br /><br /><br />

            <asp:Label runat="server" ID="membership">Type of Membership</asp:Label><asp:DropDownList runat="server" ID="typeofmembership">
                <asp:ListItem value="S" Text="silver"></asp:ListItem>
                <asp:ListItem Value="g" Text="gold"></asp:ListItem>
                <asp:ListItem Value="P" Text="Platinum"></asp:ListItem>
            </asp:DropDownList>        
            <br /><br />
            <asp:Label runat="server" ID="Label1">Payment Term</asp:Label><br />
            <asp:RadioButton runat="server" Text="Monthly" GroupName="via"/>
            <asp:RadioButton runat="server" Text="Quarterly" GroupName="via"/>
            <asp:RadioButton runat="server" Text="HalfYearly" GroupName="via"/>
            <asp:RadioButton runat="server" Text="Yearly" GroupName="via"/>
            <br /><br />

            <asp:Label runat="server" ID="Label2">Perfer Timing</asp:Label>
            <div ="Perefer Timing">
            <asp:CheckBox runat="server" ID="Morrning" Text="Morrning" />
            <asp:CheckBox runat="server" ID="Afternoon" Text="Afternoon" />
            <asp:CheckBox runat="server" ID="Evening" Text="Evening" />
            <asp:CheckBox runat="server" ID="Night" Text="Night" />
            </div>
            <asp:Button runat="server" ID="button" Text="Submit Me"/>
      
            
            
      
    </form>
</body>
</html>
